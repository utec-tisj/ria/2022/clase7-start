import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComidasComponent } from './comidas/comidas.component';
import { Comidas2Component } from './comidas2/comidas2.component';
import { Comidas3Component } from './comidas3/comidas3.component';
import { HomeComponent } from './home/home.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { NuevaComidaComponent } from './nueva-comida/nueva-comida.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  {
    path: 'test', component: TestComponent
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'comidas', component: ComidasComponent
  },
  {
    path: 'comidas2', component: Comidas2Component
  },
  {
    path: 'comidas3', component: Comidas3Component
  },
  {
    path: 'nueva-comida', component: NuevaComidaComponent
  },
  {
    path: 'noticias', component: NoticiasComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
