import { Component, OnInit } from '@angular/core';
import { Noticia } from '../models/noticia';
import { NoticiasService } from '../services/noticias.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.sass']
})
export class NoticiasComponent implements OnInit {

  public noticias: Noticia[] = [];

  constructor(private service: NoticiasService) { }

  ngOnInit(): void {
    this.service.getNoticias().subscribe({
      next: value => this.noticias = value,
      error: err => { alert('Error al cargar las noticias: ' + err) }
    });
  }
}

