import { Component, OnInit } from '@angular/core';
import { Comida } from '../models/comida';
import { ComidaService } from '../services/comida.service';

@Component({
  selector: 'app-comidas3',
  templateUrl: './comidas3.component.html',
  styleUrls: ['./comidas3.component.sass']
})
export class Comidas3Component implements OnInit {

  public titulo: string = 'Clase 7 - Comidas 3';
  public comidas: Comida[] = [];
  public miComidaFavorita: number = 0;
  
  constructor(private service: ComidaService) { }

  ngOnInit(): void {
    this.service.getAll().subscribe(
      (data) => {
        this.comidas = data;
      },
      (error) => {
        error('Error al cargar las comidas');
      }
    );
  }
}
