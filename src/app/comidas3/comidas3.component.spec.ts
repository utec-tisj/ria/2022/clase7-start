import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Comidas3Component } from './comidas3.component';

describe('Comidas3Component', () => {
  let component: Comidas3Component;
  let fixture: ComponentFixture<Comidas3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Comidas3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Comidas3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
