import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comidas',
  templateUrl: './comidas.component.html',
  styleUrls: ['./comidas.component.sass']
})
export class ComidasComponent implements OnInit {

  public titulo: string = 'Clase 7 - Comidas';
  public comidas: string[] = ['Canelones', 'Pollo al Horno',
    'Hamburguesas', 'Chivito'];
  public miComidaFavorita: number = 0;

  public values: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  // Inputs

  onKey(event: any) { // without type info
    this.values = event.target.value;
  }
}
