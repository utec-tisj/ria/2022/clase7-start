import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Comida } from '../models/comida';
import { ComidaService } from '../services/comida.service';

@Component({
  selector: 'app-nueva-comida',
  templateUrl: './nueva-comida.component.html',
  styleUrls: ['./nueva-comida.component.sass']
})
export class NuevaComidaComponent implements OnInit {

  public modelo: Comida = new Comida(0, '');

  public comidaForm: FormGroup = new FormGroup({
    id: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
  });;

  constructor(private service: ComidaService) { }

  ngOnInit(): void {
  }

  onClick() {
    this.service.addComida(new Comida(this.comidaForm.controls['id'].value, this.comidaForm.controls['name'].value));
    this.comidaForm.controls['id'].setValue(undefined);
    this.comidaForm.controls['name'].setValue(undefined);
    alert('Comida agregada correctamente.');
  }
}
