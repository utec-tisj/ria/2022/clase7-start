import { Component, OnInit } from '@angular/core';
import { Comida } from '../models/comida';

@Component({
  selector: 'app-comidas2',
  templateUrl: './comidas2.component.html',
  styleUrls: ['./comidas2.component.sass']
})
export class Comidas2Component implements OnInit {

  public titulo: string = 'Clase 7 - Comidas 2';
  public comidas: Comida[] = [
    new Comida(1, 'Canelones'), 
    new Comida(2, 'Pollo al Horno'),
    new Comida(3, 'Hamburguesas'),
    new Comida(4, 'Chivito')
  ];
  public miComidaFavorita: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
