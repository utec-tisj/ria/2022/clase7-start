import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comida } from '../models/comida';

@Injectable({
  providedIn: 'root'
})
export class ComidaService {

  public comidas: Comida[] = [
    new Comida(1, 'Canelones'),
    new Comida(2, 'Pollo al Horno'),
    new Comida(3, 'Hamburguesas'),
    new Comida(4, 'Chivito')
  ];

  public ultimoId: number = 4;

  constructor() { }

  public getAll(): Observable<Comida[]> {
    const result = new Observable<Comida[]>(observer => {
      setTimeout(() => {
        observer.next(this.comidas);
      }, 1000);
    });
    return result;
  }

  public addComida(x: Comida): Comida {
    this.comidas.push(x);
    return x;
  }
}
